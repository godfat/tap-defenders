
Estimation = Struct.new(:cost, :packs, keyword_init: true) do
  def unit_pack_price
    (cost.to_f / packs).round(2)
  end

  def to_h
    super.merge(unit_pack_price: unit_pack_price)
  end

  def +(that)
    self.class.new(cost: cost + that.cost, packs: packs + that.packs)
  end
end

def unit_pack_price n
  [4000, 1500 + 500 * n].min
end

def total_price times
  times.times.inject(0){ |sum, n| sum + unit_pack_price(n) }
end

def estimate times_per_day:, days:, rate: 1, base_packs: 1
  Estimation.new(
    cost: total_price(times_per_day) * days,
    packs: (times_per_day * days + base_packs) * rate)
end

buy_and_use_last_2_days = estimate(times_per_day: 5, days: 2, rate: 8)

puts "Buy 1 everyday and buy 5 last 2 days"
buy_and_save_1 = estimate(times_per_day: 1, days: 5, rate: 8)
p (buy_and_use_last_2_days + buy_and_save_1).to_h
puts

puts "Buy 2 everyday and buy 5 last 2 days"
buy_and_save_2 = estimate(times_per_day: 2, days: 5, rate: 8)
p (buy_and_use_last_2_days + buy_and_save_2).to_h
puts

puts "Buy 3 everyday and buy 5 last 2 days"
buy_and_save_3 = estimate(times_per_day: 3, days: 5, rate: 8)
p (buy_and_use_last_2_days + buy_and_save_3).to_h
puts

puts "Buy 4 everyday and buy 5 last 2 days"
buy_and_save_4 = estimate(times_per_day: 4, days: 5, rate: 8)
p (buy_and_use_last_2_days + buy_and_save_4).to_h
puts

puts "Buy 5 everyday and buy 5 last 2 days"
buy_and_save_5 = estimate(times_per_day: 5, days: 5, rate: 8)
p (buy_and_use_last_2_days + buy_and_save_5).to_h
puts

free_packs = estimate(times_per_day: 0, days: 5, rate: 2, base_packs: 5)

puts "Buy nothing"
p (free_packs + estimate(times_per_day: 0, days: 2, rate: 8, base_packs: 2)).to_h
puts

puts "Only buy 1 last 2 days"
p (free_packs + estimate(times_per_day: 1, days: 2, rate: 8, base_packs: 2)).to_h
puts

puts "Only buy 2 last 2 days"
p (free_packs + estimate(times_per_day: 2, days: 2, rate: 8, base_packs: 2)).to_h
puts

puts "Only buy 3 last 2 days"
p (free_packs + estimate(times_per_day: 3, days: 2, rate: 8, base_packs: 2)).to_h
puts

puts "Only buy 4 last 2 days"
p (free_packs + estimate(times_per_day: 4, days: 2, rate: 8, base_packs: 2)).to_h
puts

puts "Only buy 5 last 2 days"
p (free_packs + estimate(times_per_day: 5, days: 2, rate: 8, base_packs: 2)).to_h
puts

